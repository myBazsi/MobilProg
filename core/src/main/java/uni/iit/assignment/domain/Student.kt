package uni.iit.assignment.domain

data class Student(
    override val id: Int,
    override var name: String,
    override var office: String,
    open var phone: String
): Person(id, name, office)