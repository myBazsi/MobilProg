package uni.iit.assignment.domain

data class Employee(
    override val id: Int,
    override var name: String,
    open var position: String,
    override var office: String,
    open var phone: Int,
): Person(id, name, office)