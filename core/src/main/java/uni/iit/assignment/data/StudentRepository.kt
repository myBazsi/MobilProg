package uni.iit.assignment.data

import uni.iit.assignment.domain.Student

class StudentRepository(private val dataSource: StudentDataSource) {
    suspend fun addCustomer(customer: Student) = dataSource.addCustomer(customer)
    suspend fun removeCustomer(customer: Student) = dataSource.removeCustomer(customer)
    suspend fun updateCustomer(customer: Student) = dataSource.updateCustomer(customer)
    suspend fun fetchAllCustomer() = dataSource.fetchAllCustomer()
    suspend fun fetchCustomerById(id: Int) = dataSource.fetchCustomerById(id)

}