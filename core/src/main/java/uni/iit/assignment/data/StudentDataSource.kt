package uni.iit.assignment.data

import uni.iit.assignment.domain.Student

interface StudentDataSource {
    suspend fun addCustomer(customer: Student)
    suspend fun removeCustomer(customer: Student)
    suspend fun updateCustomer(customer: Student)
    suspend fun fetchAllCustomer() : List<Student>
    suspend fun fetchCustomerById(id: Int) : Student?

}