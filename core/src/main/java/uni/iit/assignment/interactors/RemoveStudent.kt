package uni.iit.assignment.interactors

import uni.iit.assignment.data.StudentRepository
import uni.iit.assignment.domain.Student

class RemoveStudent (private val studentRepository: StudentRepository) {
    suspend operator fun invoke(customer: Student) = studentRepository.removeCustomer(customer)
}