package uni.iit.assignment.interactors

import uni.iit.assignment.data.StudentRepository

class GetStudentById (private val studentRepository: StudentRepository) {
    suspend operator fun invoke(id: Int) = studentRepository.fetchCustomerById(id)
}