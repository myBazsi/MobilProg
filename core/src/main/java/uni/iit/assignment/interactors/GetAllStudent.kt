package uni.iit.assignment.interactors

import uni.iit.assignment.data.StudentRepository

class GetAllStudent(private val studentRepository: StudentRepository) {
    suspend operator fun invoke() = studentRepository.fetchAllCustomer()
}