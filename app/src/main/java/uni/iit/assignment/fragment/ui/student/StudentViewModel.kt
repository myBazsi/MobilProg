package uni.iit.assignment.fragment.ui.student

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import uni.iit.assignment.domain.Student
import uni.iit.assignment.interactors.GetAllStudent
import uni.iit.assignment.interactors.RemoveStudent
import uni.iit.assignment.interactors.UpdateStudent
import java.lang.Error

class StudentViewModel(private val getAllCustomer: GetAllStudent, private val removeCustomer: RemoveStudent, private val updateCustomer: UpdateStudent) : ViewModel() {

    private val _customers = MutableLiveData<List<Student>>().apply {
        value = emptyList()
    }

    private val _deletedCustomer = MutableLiveData<Student>()
    private val _updatedCustomer = MutableLiveData<Student>()

    val customers : LiveData<List<Student>> = _customers
    val deletedCustomer : LiveData<Student> = _deletedCustomer
    val updatedCustomer : LiveData<Student> = _updatedCustomer

    fun loadCustomers() {
        viewModelScope.launch {
            _customers.postValue(getAllCustomer())
        }
    }

    fun deleteCustomer(customer: Student) {
        viewModelScope.launch {
            try {
                removeCustomer(customer)
                _deletedCustomer.postValue(customer)

            } catch (err: Error)
            {
                error(err)
            }

        }
    }

    fun editStudent(customer: Student) {
        viewModelScope.launch {
            try {
                updateCustomer(customer)
                _updatedCustomer.postValue(customer)
            } catch (err : Error) {
                error(err)
            }
        }
    }
}