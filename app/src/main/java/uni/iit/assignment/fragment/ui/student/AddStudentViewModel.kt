package uni.iit.assignment.fragment.ui.student

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import uni.iit.assignment.domain.Student
import uni.iit.assignment.interactors.AddStudent

class AddStudentViewModel(private val addCustomer: AddStudent) : ViewModel() {
    private val _result = MutableLiveData<Boolean?>().apply { value = null }
    val result : LiveData<Boolean?> = _result

    fun save(customer: Student) {
        viewModelScope.launch {
            try {
                addCustomer(customer)
                _result.postValue(true)

            } catch (err: Error) {
                _result.postValue(false)
            }

        }
    }
}