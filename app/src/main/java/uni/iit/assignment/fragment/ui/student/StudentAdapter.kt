package uni.iit.assignment.fragment.ui.student

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uni.iit.assignment.databinding.StudentListItemBinding
import uni.iit.assignment.domain.Student

class StudentAdapter : RecyclerView.Adapter<StudentAdapter.CustomerViewHolder>() {

    var items : MutableList<Student> = mutableListOf()

    inner class CustomerViewHolder(val itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(position: Int) {
            val item = items[position]
            val binding = StudentListItemBinding.bind(itemView)
            binding.textStudentName.text = item.name
            binding.textStudentLocation.text = "Neptun kód: " + item.office
            binding.textStudentComment.text = item.phone
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomerViewHolder {

        val binding = StudentListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CustomerViewHolder(binding.root)
    }

    override fun getItemCount(): Int = items.count()

    override fun onBindViewHolder(holder: CustomerViewHolder, position: Int) {
        holder.bind(position)
    }

}
