package uni.iit.assignment.db.mapper

import uni.iit.assignment.db.entity.CustomerEntity
import uni.iit.assignment.domain.Student

class CustomerMapper {
    fun mapToEntity(data: Student) : CustomerEntity = CustomerEntity(
        data.id,
        data.name,
        data.office,
        data.phone
    )

    fun mapFromEntity(entity: CustomerEntity) = Student(
        entity.id,
        entity.name,
        entity.location,
        entity.comment
    )
}