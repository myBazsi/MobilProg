package uni.iit.assignment.db.datasource

import uni.iit.assignment.data.StudentDataSource
import uni.iit.assignment.db.dao.CustomerDao
import uni.iit.assignment.db.mapper.CustomerMapper
import uni.iit.assignment.domain.Student

class RoomCustomerDataSource (private val dao: CustomerDao, private val mapper: CustomerMapper) : StudentDataSource {
    override suspend fun addCustomer(customer: Student) = dao.insert(mapper.mapToEntity(customer))
    override suspend fun fetchAllCustomer() : List<Student> = dao.fetchAll().map { mapper.mapFromEntity(it) }
    override suspend fun updateCustomer(customer: Student) = dao.update(mapper.mapToEntity(customer))
    override suspend fun removeCustomer(customer: Student) = dao.delete(mapper.mapToEntity(customer))
    override suspend fun fetchCustomerById(id: Int): Student? = dao.fetchCustomerById(id)?.let { mapper.mapFromEntity(it)}

}